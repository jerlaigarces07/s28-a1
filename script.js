fetch('https://jsonplaceholder.typicode.com/todos')
    .then((response) => response.json());

fetch('https://jsonplaceholder.typicode.com/todos').then((response) => response.json()).then((json) => console.log(json.map(data => data.title)));

fetch('https://jsonplaceholder.typicode.com/todos/1')
    .then((response) => response.json()).then((json) => console.log(json.title, json.completed));;

fetch('https://jsonplaceholder.typicode.com/todos', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json'
        },
        body: JSON.stringify({
            userId: 1,
            title: 'New todo',
            completed: true 
        })
    }).then((response) => response.json());

fetch('https://jsonplaceholder.typicode.com/todos/2', {
    method: 'PUT',
    headers: {
        'Content-type': 'application/json'
    },
    body: JSON.stringify({
        id: 1,
        title: 'Update todo to completed',
        completed: true,
        userId: 1
    })
}).then((response) => response.json());

fetch('https://jsonplaceholder.typicode.com/todos/3', {
    method: 'PUT',
    headers: {
        'Content-type': 'application/json'
    },
    body: JSON.stringify({
        id: 3,
        title: 'Updated list items',
        description: 'Please finish before 5pm',
        status: true,
        dateCompleted: 'October 27, 2021',
        userId: 1
    })
}).then((response) => response.json());

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH',
    headers: {
        'Content-type': 'application/json'
    },
    body: JSON.stringify({
        title: 'wash the dishes'
    })
}).then((response) => response.json());

fetch('https://jsonplaceholder.typicode.com/todos/6', {
    method: 'PUT',
    headers: {
        'Content-type': 'application/json'
    },
    body: JSON.stringify({
        userId: 1,
        id: 6,
        title: 'qui ullam ratione quibusdam voluptatem quia omnis',
        completed: true,
        dateCompleted: 'October 26, 2021'
    })
}).then((response) => response.json());

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'DELETE',
});